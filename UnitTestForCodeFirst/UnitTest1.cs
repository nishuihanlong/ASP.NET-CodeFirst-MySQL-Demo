﻿using System;
using CodeFirstMysql;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestForCodeFirst
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            InitProject.InitData();
        }

        [TestMethod]
        public void TestInclude()
        {
            using (MyContext context = new MyContext())
            {
                Province province = new Province();


                province.ProvinceName = "HeNan";

                context.Provinces.Add(province);
                City city = new City();
                city.CityName = "ZhengZhou";
                city.PostCode = "450001";
                //city.ProvinceId = province.Id;
                City city1 = new City();
                city1.CityName = "KaiFeng";
                city1.PostCode = "450002";
                //city1.ProvinceId = province.Id;
                context.Cities.Add(city);
                context.Cities.Add(city1);
                context.SaveChanges();
            }
        }
    }
}
