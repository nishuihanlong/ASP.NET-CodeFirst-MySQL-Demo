﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodeFirstMysql
{
    public class InitProject
    {
        public static void InitData()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<MyContext>());
            var context = new MyContext();
            context.Users.Add(new User { UserName = "EF6-MySQL-Code-First" });
            context.SaveChanges();
        }
    }
}