﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodeFirstMysql
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitData()
        {
            using (MyContext context = new MyContext())
            {
                context.Users.Add(new User {UserName = "EF6MySQLCodeFirst"});

                Province province = new Province();
                City city = new City();

                province.ProvinceName = "HeNan";

                context.Provinces.Add(province);
                city.CityName = "ZhengZhou";
                city.PostCode = "450001";
                city.ProvinceId = province.Id;
                context.Cities.Add(city);

                context.SaveChanges();

                List<Province> list= context.Provinces.Include(c => c.Cities).ToList();
                foreach (Province item in list)
                {
                    Response.Write(item.Cities.FirstOrDefault().CityName);
                }
            }
        }
    }
}