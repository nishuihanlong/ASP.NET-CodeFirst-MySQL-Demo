﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using MySql.Data.Entity;

namespace CodeFirstMysql
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MyContext : DbContext
    {
        public MyContext()
            : base("name=dbconnetion")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MyContext>());
        }


        public DbSet<User> Users { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<City> Cities { get; set; }
    }
}
